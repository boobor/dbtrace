package john.walker.filter;

import java.util.List;

/**
 * 可配置的过滤器
 *
 * @author 30san
 *
 */
public class ConfigurableStackTraceFilter implements StackTraceFilter {
	private List<String> stackTraceFilterWords;

	public ConfigurableStackTraceFilter(List<String> stackTraceFilterWords) {
		this.stackTraceFilterWords = stackTraceFilterWords;
	}

	@Override
	public boolean accpet(String className, String methodName) {
		for(String word : stackTraceFilterWords) {
			if(word == null || word.trim().isEmpty()) {
				return true;
			}
			if(word.endsWith("*") && word.length() > 1) {
				if(word.startsWith("*") && word.length() > 2) {
					if(className.contains(word.substring(1).substring(0, word.length()-1))) {
						return false;
					}
				} else if(className.startsWith(word.substring(0, word.length()-1))) {
					return false;
				}
			} else if(word.startsWith("*") && word.length() > 1) {
				if(word.endsWith(word.substring(1))) {
					return false;
				}
			} else {
				if(className.equals(word)) {
					return false;
				}
			}
		}
		return true;
	}
}
